import { Torneo } from "../entity/Torneo";
import { Usuario } from "../entity/Usuario";
import { genSaltSync, hashSync } from "bcrypt";
import { saltHash } from "../env";
import { TorneoUsuario } from "../entity/TorneoUsuario";
import { getRepository } from "typeorm";
import { Resolucion } from "../entity/Resolucion";

export const generateToken = async (torneo: Torneo, usuario: Usuario) => {
  try {
    let hashEncontrado = false;
    let hash:string;
    do {
      let salt = genSaltSync(saltHash);
      hash = hashSync(JSON.stringify({ ...torneo, ...usuario }), salt);
      hash = hash.substring(0,31)
      let buscarHash = await getRepository(TorneoUsuario).findOne({
        where: { token: hash }
      });
      if (!buscarHash) {
        hashEncontrado = true;
      }
    } while (!hashEncontrado);
    return hash;
  } catch (error) {
    throw error;
  }
};

export const generateTokenResolucion = async (data:any) => {
  try{
    let hashEncontrado = false;
    let hash:string;
    do {
      let salt = genSaltSync(saltHash);
      hash = hashSync(JSON.stringify({...data}), salt);
      hash = hash.substring(0,31);
      let buscarHash = await getRepository(Resolucion).findOne({
        where: {token:hash}
      });
      if(!buscarHash){
        hashEncontrado = true;
      }
    }while(!hashEncontrado)
    return hash;
  }catch(error){ 
    throw error;
  }
}


export function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

