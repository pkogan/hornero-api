
import { cambiarEstadoTorneo } from './cambiarEstado'
import {CronJob} from 'cron';

export function ejecutarCron1(){
    //Los estados de los torneos que cambian cada dia
    let job = new CronJob( '* * * * *', cambiarEstadoTorneo);
    job.start();
}


