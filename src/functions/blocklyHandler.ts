import { Server, Socket } from "socket.io";
import { verify } from "jsonwebtoken";
import { getRandomColor } from "./utiles";
import { privateKey } from "../env";
let salas = {};

export const blocklyHandler = (io: Server) => {
  io.on("connection", (socket: Socket) => {
    socket.on("conexionBloques", (data: blockHandler) => {
      console.log(data);
      let payload;
      try {
        payload = verify(data.usuario, privateKey);
      } catch (error) {
        return;
      }
      const usuario = payload["usuario"];
      socket.join(data.token);
      if (!salas[data.token]) {
        const nuevaSala = new salaHandler(data.token);
        const userSala = nuevaSala.agregarUsuario(
          usuario.nombreUsuario,
          socket.id
        );
        salas[data.token] = nuevaSala;
        socket.emit("getColor", userSala.color);
        io.to(nuevaSala.tokenSala).emit(
          "getUsuarios",
          nuevaSala.usuariosConectados
        );
      } else {
        const sala: salaHandler = salas[data.token];
        if (!sala.existeUsuario(usuario.nombreUsuario))
          sala.agregarUsuario(usuario.nombreUsuario, socket.id);
        const mensaje = {
          token: data.token,
          xml: sala.xml,
          usuariosConectados: sala.usuariosConectados,
        };
        //envio color del jugador
        socket.emit("getColor", sala.getUsuario(usuario.nombreUsuario).color);
        //actualizo su espacio de trabajo
        socket.emit("updateXml", mensaje);
        //le aviso a todos que se conecta un nuevo usuario
        io.to(sala.tokenSala).emit("getUsuarios", sala.usuariosConectados);
        sala.usuariosConectados.forEach((e) => {
          let bloqueSeleccionado: marcaControler = {
            color: e.color,
            idBloque: e.bloqueSeleccionado,
            idMarca: e.idMarcador,
          };
          socket.emit("actualizarBloque", bloqueSeleccionado);
        });
      }
    });

    socket.on("updateXml", (data: blockHandler) => {
      let payload = verify(data.usuario, privateKey);
      const usuario = payload["usuario"];

      const sala: salaHandler = salas[data.token];
      if (sala) {
        //por si tiene varias sesiones abiertas
        if (!sala.existeUsuario(usuario.nombreUsuario)) {
          sala.agregarUsuario(usuario.nombreUsuario, socket.id);
          io.to(sala.tokenSala).emit("getUsuarios", sala.usuariosConectados);
        }
        socket.emit("getColor", sala.getUsuario(usuario.nombreUsuario).color);
        socket.to(data.token).emit("updateXml", data);
        sala.xml = data.xml;

        sala.usuariosConectados.forEach((e) => {
          let bloqueSeleccionado: marcaControler = {
            color: e.color,
            idBloque: e.bloqueSeleccionado,
            idMarca: e.idMarcador,
          };
          socket.emit("actualizarBloque", bloqueSeleccionado);
        });
      }
    });

    socket.on("getAllSelects", (data: blockHandler) => {
      const sala = salas[data.token];
      if (sala) {
        sala.usuariosConectados.forEach((e) => {
          let bloqueSeleccionado: marcaControler = {
            color: e.color,
            idBloque: e.bloqueSeleccionado,
            idMarca: e.idMarcador,
          };
          socket.emit("actualizarBloque", bloqueSeleccionado);
        });
      }
    });

    socket.on("seleccionarBloque", (data: blockHandler) => {
      let payload = verify(data.usuario, privateKey);
      const usuario = payload["usuario"];

      const sala: salaHandler = salas[data.token];
      if (sala) {
        if (!sala.existeUsuario(usuario.nombreUsuario)) {
          sala.agregarUsuario(usuario.nombreUsuario, socket.id);
          io.to(sala.tokenSala).emit("getUsuarios", sala.usuariosConectados);
        }
        socket.emit("getColor", sala.getUsuario(usuario.nombreUsuario).color);
        const userSala = sala.agregarBloqueSeleccionado(
          usuario.nombreUsuario,
          data.bloqueMarca,
          data.idMarca
        );
        let bloqueSeleccionado: marcaControler = {
          color: userSala.color,
          idBloque: data.bloqueMarca,
          idMarca: data.idMarca,
        };
        socket.to(data.token).emit("actualizarBloque", bloqueSeleccionado);
      }
    });

    socket.on("enviarMensaje",(data:chatHandler) =>{
      let payload = verify(data.usuario, privateKey);
      const usuario:usuarioSala = payload["usuario"];

      const sala: salaHandler = salas[data.token];
      if (sala) {
        if (!sala.existeUsuario(usuario.nombreUsuario)) {
          sala.agregarUsuario(usuario.nombreUsuario, socket.id);
          io.to(sala.tokenSala).emit("getUsuarios", sala.usuariosConectados);
        }
        sala.chat.agregarMensaje(data.mensaje);
        socket.to(data.token).emit("recibirMensaje",data.mensaje);
      }

    })

    socket.on("disconnect", () => {
      for (const p in salas) {
        salas[p].usuariosConectados = salas[p].usuariosConectados.filter(
          (e) => e.idSocket !== socket.id
        );
        io.to(salas[p].tokenSala).emit(
          "getUsuarios",
          salas[p].usuariosConectados
        );
      }
    });
  });
};

interface chatHandler{
  mensaje?:Mensaje,
  usuario?:string,
  token?:string,
}

interface blockHandler {
  token: string;
  xml: string;
  usuario?: string;
  usuariosConectados?: usuarioSala[];
  idMarca?: string;
  bloqueMarca?: string;
}

interface marcaControler {
  idBloque: string;
  idMarca: string;
  color: string;
}

interface usuarioSala {
  idSocket?: string;
  nombreUsuario?: string;
  color?: string;
  idMarcador?: string;
  bloqueSeleccionado?: string;
}
interface Mensaje {
  mensaje:string;
  usaurio:usuarioSala;
}
class Chat {
  public mensajes:Mensaje[];

  constructor(){
    this.mensajes = [];
  }
  agregarMensaje(mensaje:Mensaje){
    if(this.mensajes.length > 20){
      this.mensajes.pop()
    }
    this.mensajes.unshift(mensaje);
  }
}
class salaHandler {
  public tokenSala: string;
  public usuariosConectados: usuarioSala[] = [];
  public xml: string;
  public chat:Chat;

  constructor(token: string) {
    this.tokenSala = token;
    this.xml = "";
    this.chat = new Chat();
  }
  agregarBloqueSeleccionado(nombre: string, bloque: string, marcaId: string) {
    const usuario = this.usuariosConectados.find(
      (e) => e.nombreUsuario === nombre
    );
    usuario.bloqueSeleccionado = bloque;
    usuario.idMarcador = marcaId;
    return usuario;
  }
  agregarUsuario(nombre: string, idSocket: string): usuarioSala {
    let nuevoUsuario = {
      nombreUsuario: nombre,
      color: getRandomColor(),
      idSocket,
      bloqueSeleccionado: null,
    };
    this.usuariosConectados.push(nuevoUsuario);
    return nuevoUsuario;
  }
  getUsuario(nombre: string) {
    return this.usuariosConectados.find((e) => e.nombreUsuario === nombre);
  }
  existeUsuario(nombre: string) {
    return (
      this.usuariosConectados.filter((e) => e.nombreUsuario === nombre).length >
      0
    );
  }
  eliminarSocket(idSocket: string) {
    this.usuariosConectados.filter((e) => e.idSocket !== idSocket);
  }
}
