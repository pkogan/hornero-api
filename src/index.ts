import "reflect-metadata";
let cors = require("cors");
import { createConnection } from "typeorm";
import Server from "./server/Server";
import bodyParser = require("body-parser");
import * as routes from  './routes/routes.index';
import { port } from './env'
import {ejecutarCron1} from './functions/cronJobs'
import {blocklyHandler} from "./functions/blocklyHandler"
createConnection()
.then(con => {
    const server = Server.init(port);

    //Cron del servidor para actualizar cosas 
    ejecutarCron1();
    server.app.use(cors());
    server.app.use(bodyParser.json({ limit: "50mb" }));
    server.app.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));
    server.app.use('/api',routes);
    blocklyHandler(server.socketIo);

    server.start(() => {
      console.log(`servidor corriendo en el puerto http://localhost:${port}/api`);
    });
  })
  .catch(error =>
    console.log({ error, mg: "No se pudo conectar a la base de datos" })
  );
