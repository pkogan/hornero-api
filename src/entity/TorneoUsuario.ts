import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Torneo} from "./Torneo";
import {Usuario} from "./Usuario";


@Entity("torneousuario" ,{schema:"hornero" } )
@Index("idTorneo",["torneo","usuario",])
@Index("idUsuario",["usuario",])
export class TorneoUsuario {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idTorneoUsuario"
        })
    idTorneoUsuario:number;
        

   
    @ManyToOne(()=>Torneo, (torneo: Torneo)=>torneo.torneosUsuarios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idTorneo'})
    torneo:Torneo | null;


   
    @ManyToOne(()=>Usuario, (usuario: Usuario)=>usuario.torneosUsuarios,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idUsuario'})
    usuario:Usuario | null;


    @Column("int",{ 
        nullable:false,
        default: () => "'0'",
        name:"Puntos"
        })
    puntos:number;
        

    @Column("bigint",{ 
        nullable:false,
        default: ()=>"'0'",
        name:"Tiempo"
        })
    tiempo:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:32,
        name:"Token"
        })
    token:string;
        
}
