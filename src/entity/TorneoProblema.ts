import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Problema} from "./Problema";
import {Torneo} from "./Torneo";


@Entity("torneoproblema" ,{schema:"hornero" } )
@Index("idProblema",["problema","torneo",])
@Index("idTorneo",["torneo"])
export class TorneoProblema {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idTorneoProblema"
        })
    idTorneoProblema:number;
        

   
    @ManyToOne(()=>Problema, (problema: Problema)=>problema.torneoproblemas,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idProblema'})
    problema:Problema | null;


   
    @ManyToOne(()=>Torneo, (torneo: Torneo)=>torneo.torneosProblemas,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idTorneo'})
    torneo:Torneo | null;


    @Column("int",{ 
        nullable:false,
        name:"Orden"
        })
    orden:number;
        
}
