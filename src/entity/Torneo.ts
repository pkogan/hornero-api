import {Column,Entity,Index,JoinColumn,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn} from "typeorm";
import {EstadoTorneo} from "./EstadoTorneo";
import {TipoTorneo} from "./TipoTorneo";
import {Resolucion} from "./Resolucion";
import {TorneoProblema} from "./TorneoProblema";
import {TorneoUsuario} from "./TorneoUsuario";
import { Usuario } from "./Usuario";


@Entity("torneo" ,{schema:"hornero" } )
@Index("idEstado",["estado",])
@Index("idTipo",["tipo",])
export class Torneo {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idTorneo"
        })
    idTorneo:number;
        

    @Column("varchar",{ 
        nullable:false,
        name:"Nombre"
        })
    nombre:string;
        

    @Column("text",{ 
        nullable:false,
        name:"Descripcion"
        })
    descripcion:string;
        

    @Column("datetime",{ 
        nullable:false,
        name:"FechaInicio"
        })
    fechaInicio:Date;
        

    @Column("datetime",{ 
        nullable:false,
        name:"FechaFin"
        })
    fechaFin:Date;
        
    @ManyToOne(()=>Usuario,(creador:Usuario)=> creador.torneosCreados,{onDelete:'RESTRICT',onUpdate:"RESTRICT"})
    @JoinColumn({name:"creador"})
    creador:Usuario
   
    @ManyToOne(()=>EstadoTorneo, (estadotorneo: EstadoTorneo)=>estadotorneo.torneos,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idEstado'})
    estado:EstadoTorneo | null;


   
    @ManyToOne(()=>TipoTorneo, (tipotorneo: TipoTorneo)=>tipotorneo.torneos,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idTipo'})
    tipo:TipoTorneo | null;


   
    @OneToMany(()=>Resolucion, (resolucion: Resolucion)=>resolucion.torneo,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    resoluciones:Resolucion[];
    

   
    @OneToMany(()=>TorneoProblema, (torneoproblema: TorneoProblema)=>torneoproblema.torneo,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    torneosProblemas:TorneoProblema[];
    

   
    @OneToMany(()=>TorneoUsuario, (torneousuario: TorneoUsuario)=>torneousuario.torneo,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    torneosUsuarios:TorneoUsuario[];
}
