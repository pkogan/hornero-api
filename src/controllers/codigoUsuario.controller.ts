import {  Request, Response } from "express";
import { CodigoUsuario } from "../entity/CodigoUsuario";
import { getRepository } from "typeorm";
import { Usuario } from "../entity/Usuario";

export const createCodigo = async (req:Request,res:Response)=>{
    try{
        let usuario:Usuario = req['usuario'];
        const codigoRepository = getRepository(CodigoUsuario);
        const nuevoCodigo = codigoRepository.create(req.body as CodigoUsuario) ;
        nuevoCodigo.usuario = usuario;
        console.log(nuevoCodigo)
        const result = codigoRepository.save(nuevoCodigo)
        res.json(result)
    } catch (error) {
        res.status(500).json({ error });
      }
}
