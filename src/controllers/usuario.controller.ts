import { Request, Response } from "express";
import { Usuario } from "../entity/Usuario";
import { getRepository } from "typeorm";


export const getUsuarios =  async (req: Request, res: Response) => {
    try {
      let select = req.query.select ? JSON.parse((req.query.select) as string ) : ""
      let relations = req.query.relations ? JSON.parse((req.query.relations) as string) : ""
      let result = await getRepository(Usuario).find({select:select,relations:relations});
      res.json(result);
    } catch (error) {
      console.log(error)
      res.status(500).json({ error });
    }
}

export const getUsuario = async (req:Request,res:Response) => {
    try {
      let id = req.params.id;
      let select = req.query.select ? JSON.parse((req.query.select) as string ) : ""
      let relations = req.query.relations ? JSON.parse((req.query.relations) as string) : ""
      console.log(relations)
      console.log(id);
      let result = await getRepository(Usuario).findOne({select:select,relations:relations,where:[{idUsuario:id}]});
      res.json(result);
    } catch (error) {
      console.log(error)
      res.status(500).json({ error });
    }
}

export const createUsuario = async (req:Request,res:Response)=>{
    try{
        const userRepository = getRepository(Usuario);
        const nuevoUsuario = userRepository.create(req.body)
        const result = userRepository.save(nuevoUsuario)
        res.json(result)
    } catch (error) {
        res.status(500).json({ error });
      }
}

export const emailRegistrado =  async (req: Request, res: Response)=>{
    let email = req.body.email
    const result = await getRepository(Usuario).findOne({where:[{email:email}]});
    if(!result){
        res.json({estaEnUso:false})
    }else{
        res.json({estaEnUso:true})
    }

}

export const usuarioRegistrado = async (req: Request, res: Response)=>{
  let nombreUsuario = req.body.username
  const result = await getRepository(Usuario).findOne({where:[{nombreUsuario:nombreUsuario}]});
  if(!result){
      res.json({estaEnUso:false})
  }else{
      res.json({estaEnUso:true})
  }

}
