import { Request, Response } from "express";
import { TipoTorneo } from "../entity/TipoTorneo";
import { getRepository } from "typeorm";

export const getTiposTorneos = async (req: Request, res: Response) => {
    try {
        let select = req.query.select ? JSON.parse((req.query.select) as string ) : ""
        let relations = req.query.relations ? JSON.parse((req.query.relations) as string) : ""
             let result = await getRepository(TipoTorneo).find({select:select,relations:relations}) ;
      res.json(result);
    } catch (error) {
      res.status(500).json({ error });
    }
};

export const getTipoTorneo = async (req:Request,res:Response) => {
    try{
        let id = req.params.id;
        let select = req.query.select ? JSON.parse((req.query.select) as string ) : ""
        let relations = req.query.relations ? JSON.parse((req.query.relations) as string) : ""
             if(Number(id)){
            let result = await getRepository(TipoTorneo).findOne({select:select,relations:relations,where:[{idLenguaje:id}]});
            res.json(result)
        }else{
            res.status(400).json({msg:"Id invalido"})
        }
    }catch(error){
        res.status(500).json(error)
    }
}

export const createEstado = async (req:Request,res:Response) =>{
    res.send({msg:"Not implemented yet"})
}