import { Request, Response } from "express";
import { Torneo } from "../entity/Torneo";
import { getRepository } from "typeorm";
import { Usuario } from "../entity/Usuario";
import { TorneoUsuario } from "../entity/TorneoUsuario";
import { generateToken } from "../functions/utiles";
import { TorneoProblema } from "../entity/TorneoProblema";
import { cambiarEstadoTorneo } from "../functions/cambiarEstado";

export const getTorneos = async (req: Request, res: Response) => {
  try {
    let select = req.query.select ? JSON.parse(req.query.select as string) : "";
    let relations = req.query.relations
      ? JSON.parse(req.query.relations as string)
      : "";
    let result = await getRepository(Torneo).find({
      select: select,
      relations: relations,
      order: { fechaFin: "DESC" },
    });
    res.json(result);
  } catch (error) {
    res.status(500).json({ error });
  }
};

export const getTorneo = async (req: Request, res: Response) => {
  try {
    let id = req.params.id;
    let select = req.query.select ? JSON.parse(req.query.select as string) : "";
    let relations = req.query.relations
      ? JSON.parse(req.query.relations as string)
      : "";
    let result;
    if (Number(id)) {
      result = await getRepository(Torneo).findOne({
        select: select,
        relations: relations,
        where: [{ idTorneo: id }],
      });
      if (!result) {
        res.status(404).json({ msg: "torneo no encontrado" });
      } else {
        res.json(result);
      }
    } else {
      res.status(400).json({ msg: "Id invalido" });
    }
  } catch (error) {
    res.status(500).json({ error });
  }
};

export const createTorneo = async (req: Request, res: Response) => {
  try {
    let usuario: Usuario = req["usuario"];
    const torneoRepository = getRepository(Torneo);

    const nuevoTorneo = torneoRepository.create({ ...req.body } as object);

    nuevoTorneo.creador = usuario;
    const result = await torneoRepository.save(nuevoTorneo);
    for (let i = 0; i < nuevoTorneo.torneosProblemas.length; i++) {
      let torneoNuevo = new Torneo();
      torneoNuevo.idTorneo = result.idTorneo;
      nuevoTorneo.torneosProblemas[i].torneo = torneoNuevo;
      await getRepository(TorneoProblema).save(nuevoTorneo.torneosProblemas[i]);
    }
    res.json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error });
  } finally {
    cambiarEstadoTorneo()
      .then(() => {})
      .catch((error) => console.log(error));
  }
};

export const editTorneo = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const torneoRepository = getRepository(Torneo);
    const torneoProblemaRepository = getRepository(TorneoProblema);
    if (Number(id)) {
      const torneoAEditar = await getRepository(Torneo).findOne(id, {
        relations: ["torneosProblemas"],
      });
      if (torneoAEditar) {
        const torneoEditado = torneoRepository.merge(torneoAEditar, req.body);
        const result = await torneoRepository.save(torneoEditado);
        torneoAEditar.torneosProblemas.forEach((t: TorneoProblema) => {
          let torneo = torneoRepository.create({
            idTorneo: torneoAEditar.idTorneo,
          } as Torneo);
          t.torneo = torneo;
          console.log(t);
          torneoProblemaRepository.save(t);
        });
        res.json(result);
      } else {
        res.status(404).json({ msg: "torneo no encontrado" });
      }
    } else {
      res.status(400).json({ msg: "Id invalido" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ error });
  }
};

export const registrarTorneo = async (req: Request, res: Response) => {
  try {
    const id = req.params.id;
    const torneoRepository = getRepository(Torneo);
    const torneoUsuarioRepository = getRepository(TorneoUsuario);
    if (Number(id)) {
      let torneoAincribirse = await torneoRepository.findOne(id);
      let torneoUsuario = new TorneoUsuario();
      if (torneoAincribirse) {
        if (!(await estaRegistrado(torneoAincribirse, req["usuario"]))) {
          torneoUsuario.torneo = torneoAincribirse;
          torneoUsuario.usuario = req["usuario"];
          const token = await generateToken(torneoAincribirse, req["usuario"]);
          torneoUsuario.token = token;
          const resp = await torneoUsuarioRepository.save(torneoUsuario);
          res.json(resp);
        } else {
          res
            .status(400)
            .json({ msg: "El usuario esta inscripto en el torneo" });
        }
      } else {
        res.status(404).json({ msg: "torneo no encontrado" });
      }
    } else {
      res.status(400).json({ msg: "Id invalido" });
    }
  } catch (error) {
    res.status(500).json({ error, msg: "error interno" });
  }
};

async function estaRegistrado(torneo: Torneo, usuario: Usuario) {
  const torneoUsuarioRepository = getRepository(TorneoUsuario);
  const usuarioRegistrado = await torneoUsuarioRepository.findOne({
    where: { torneo: torneo, usuario: usuario },
  });
  if (usuarioRegistrado) {
    return true;
  } else {
    return false;
  }
}
