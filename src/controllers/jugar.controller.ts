import { Request, Response } from "express";
import { getRepository, getManager } from "typeorm";
import { TorneoUsuario } from "../entity/TorneoUsuario";
import { generateTokenResolucion } from "../functions/utiles";
import { random } from "lodash";
import { Resolucion } from "../entity/Resolucion";
import { ESTADO_TORNEO } from "../entity/EstadoTorneo";
import {ESTADO_RESOLUCION,EstadoResolucion} from "../entity/EstadoResolucion";

export const solicitud = async (req: Request, res: Response) => {
  try {
    /** token solicitud*/
    const token = req.query.token;
    let problemaOrden: number;
    /** Orden Del Problema*/
    problemaOrden = Number(req.query.problema);
    if (!problemaOrden) {
      throw {
        error: "El tipo de datos de problema tiene que ser entero",
        status: 400,
      };
    }
    let problemasAux = await getRepository<TorneoUsuario>(TorneoUsuario)
      .createQueryBuilder("tu")
      .innerJoinAndSelect("tu.torneo", "torneo")
      .innerJoinAndSelect("tu.usuario", "usuario")
      .innerJoinAndSelect("torneo.torneosProblemas", "torneoproblema")
      .innerJoinAndSelect("torneoproblema.problema", "problema")
      .innerJoinAndSelect("torneo.estado", "estadotorneo")
      .innerJoinAndSelect("problema.soluciones", "solucion")
      .where("torneoproblema.Orden = :orden and tu.Token = :token", {
        orden: problemaOrden,
        token: token,
      })
      .getOne();
    if (!problemasAux) {
      throw {
        error: "El token o numero de problema es incorrecto",
        status: 400,
      };
    }
    if (problemasAux.torneo.estado.idEstado !== ESTADO_TORNEO.EnProceso) {
      throw { error: "El torneo no esta activo", status: 400 };
    }

    let soluciones =
      problemasAux.torneo.torneosProblemas[0].problema.soluciones;

    if (!soluciones) {
      throw {
        error: "El problema no tiene ninguna resolucion asociado",
        status: 400,
      };
    }

    let solucionElegida = soluciones[random(soluciones.length - 1)];
    delete problemasAux.torneo.torneosProblemas[0].problema.soluciones;
    
    const resolucionRepo = getRepository<Resolucion>(Resolucion);

    let resolu: Resolucion = resolucionRepo.create({
      torneo: problemasAux.torneo,
      fechaSolicitud: new Date().getTime().toString(),
      estado: { idEstado: ESTADO_RESOLUCION.Iniciado },
      token: await generateTokenResolucion({ problemasAux }),
      usuario: problemasAux.usuario,
      problema: problemasAux.torneo.torneosProblemas[0].problema,
      solucion: solucionElegida,
    });

    await resolucionRepo.save(resolu);

    return res.json({
      parametrosEntrada: solucionElegida.parametrosEntrada,
      token: resolu.token,
      /*
      opciones: array de opciones posibles random donde 1 es la correcta,
      nombreProblema
      enunciado
      */
    });
  } catch (error) {
    const status = error.status || 500;
    console.log(error);
    res.status(status).json(error);
  }
};

export const respuesta = async (req: Request, res: Response) => {
  try {
    const token = req.query.tokenSolicitud;
    const solucion = req.query.solucion as string;
    const tiempoActual: number = new Date().getTime();
    const repositorioResolucion = getRepository<Resolucion>(Resolucion);
    let resolucionRespuesta = await repositorioResolucion
      .createQueryBuilder("r")
      .innerJoinAndSelect("r.torneo", "torneo")
      .innerJoinAndSelect("r.usuario", "usuario")
      .innerJoinAndSelect("r.solucion", "solucion")
      .innerJoinAndSelect("r.problema", "problema")
      .innerJoinAndSelect("r.estado", "estadoresolucion")
      .innerJoinAndSelect("torneo.estado", "estadotorneo")
      .where("r.Token=:token and torneo.idEstado = :estadoTorneo", {
        token,
        estadoTorneo: ESTADO_TORNEO.EnProceso,
      })
      .getOne();

    if (!resolucionRespuesta) {
      throw { error: "El token es invalido", status: 400 };
    } else if (resolucionRespuesta.respuesta) {
      throw { error: "Esta resolcion ya esta respondida", status: 400 };
    } else {
      let nuevoEstado = resolucionRespuesta.estado.idEstado;
      if (resolucionRespuesta.solucion.salida === solucion) {
        nuevoEstado = ESTADO_RESOLUCION.Correcta;
      } else {
        nuevoEstado = ESTADO_RESOLUCION.Incorrecta;
      }
      repositorioResolucion.merge(resolucionRespuesta, {
        estado: { idEstado: nuevoEstado },
        respuesta: solucion,
        fechaRespuesta: tiempoActual.toString(),
      });
      const result = await repositorioResolucion.save(resolucionRespuesta);
      let resolucionesCorrectas = await getRepository(Resolucion).find({
        relations: ["usuario"],
        where: {
          estado: {
            idEstado: ESTADO_RESOLUCION.Correcta,
          },
          usuario: resolucionRespuesta.usuario,
          problema: resolucionRespuesta.problema,
          torneo: resolucionRespuesta.torneo,
        },
      });
      console.log(resolucionesCorrectas.length)
      if (resolucionesCorrectas.length === 3 && nuevoEstado === ESTADO_RESOLUCION.Correcta) {
        nuevoEstado = ESTADO_RESOLUCION.PuntoGanado;
        //obtengo el puntaje del usuario en el torneo si es su tercer respuesta
        let torneoUsuario = await getRepository(TorneoUsuario).findOne({
          where: {
            usuario: {
              idUsuario: resolucionRespuesta.usuario.idUsuario,
            },
            torneo: {
              idTorneo: resolucionRespuesta.torneo.idTorneo,
            },
          },
        });

        getRepository(TorneoUsuario).merge(torneoUsuario, {
          puntos: torneoUsuario.puntos + 1,
        });
        await getRepository(TorneoUsuario).save(torneoUsuario);
      }else if(resolucionesCorrectas.length > 3 && nuevoEstado === ESTADO_RESOLUCION.Correcta ){
          nuevoEstado = ESTADO_RESOLUCION.ProblemaSolucionadoCorrecta;
      }
      const estadoResultado = await getRepository(EstadoResolucion).findOne(
        nuevoEstado
      );
      return res.json(estadoResultado);
    }
  } catch (error) {
    let status = error.status || 500;
    console.log(error);
    res.status(status).json(error);
  }
};
