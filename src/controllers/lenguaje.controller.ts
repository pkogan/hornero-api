import { Router, Request, Response } from "express";
import { Lenguaje } from "../entity/Lenguaje";
import { getRepository } from "typeorm";

export const getLenguajes = async (req: Request, res: Response) => {
    try {
        let select = req.query.select ? JSON.parse((req.query.select) as string ) : ""
        let relations = req.query.relations ? JSON.parse((req.query.relations) as string) : ""
            let result = await getRepository(Lenguaje).find({select:select,relations:relations}) ;
      res.json(result);
    } catch (error) {
      res.status(500).json({ error });
    }
};

export const getLenguaje = async (req:Request,res:Response) => {
    try{
        let id = req.params.id;
        let select = req.query.select ? JSON.parse((req.query.select) as string ) : ""
        let relations = req.query.relations ? JSON.parse((req.query.relations) as string) : ""
            if(Number(id)){
            let result = await getRepository(Lenguaje).findOne({select:select,relations:relations,where:[{idLenguaje:id}]});
            res.json(result)
        }else{
            res.status(400).json({msg:"Id invalido"})
        }
    }catch(error){
        res.status(500).json(error)
    }
}

export const createLenguaje = async (req:Request,res:Response) =>{
    res.send({msg:"Not implemented yet"})
}