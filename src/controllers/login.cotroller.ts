import { Request, Response } from "express";
import { privateKey, expiracionToken } from "../env";
const jwt = require("jsonwebtoken");
import { compareSync } from "bcrypt";
import { getManager, getRepository } from "typeorm";
import { Usuario as usuarioRepo } from "../entity/Usuario";

export const login = async (req: Request, res: Response) => {
  try {
    let { usuario, pass } = req.body;
    const usuarioConectado = await getRepository(usuarioRepo).findOne({
      relations: ["rol", "torneosUsuarios", "torneosUsuarios.torneo"],
      where: { nombreUsuario: usuario }
    });
    const resp = await getManager()
      .query(`SELECT NombreUsuario, Email, Institucion,Clave, r.idRol,r.Rol
                  FROM usuario u inner join rol r on u.idRol = r.idRol 
                  WHERE NombreUsuario='${usuario}'`);

    if (resp.length === 0 || !compareSync(pass, resp[0].Clave)) {
      res.status(403).json({ msg: "Credenciales incorrectas" });
    } else {
      let fecha = new Date();
      fecha.setHours(fecha.getHours() + 1);
      const usuario: object = {
        idUsuario: usuarioConectado.idUsuario,
        institucion: usuarioConectado.institucion,
        nombreUsuario: usuarioConectado.nombreUsuario,
        descripcion: usuarioConectado.descripcion,
        email: usuarioConectado.email,
        rol: usuarioConectado.rol
      };
      const credenciales = {
        fechaFin: fecha,
        usuario: usuarioConectado,
        token: jwt.sign({ usuario }, privateKey, { expiresIn: expiracionToken })
      };
      res.json(credenciales);
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({ msg: "Ubo un error en el servidor " + error });
  }
};
