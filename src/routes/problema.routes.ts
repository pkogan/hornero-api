import { Router } from "express";
import {getProblema,getProblemas, crearProblema} from "../controllers/problema.controller";
import { isAdmin } from '../middleware/auth'

const router = Router();

router.get("/", getProblemas);
router.get("/:id",getProblema);
router.post("/",[isAdmin],crearProblema);
/*router.put("/",editLenguaje);
*/

export = router;
