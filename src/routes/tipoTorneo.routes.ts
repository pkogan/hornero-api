import { Router } from "express";
import {getTipoTorneo,getTiposTorneos,createEstado} from "../controllers/tipoTorneo.controller"
const router = Router();

router.get("/",getTiposTorneos);
router.get("/:id",getTipoTorneo);
router.post("/",createEstado);
/*
router.put("/",editLenguaje);
*/

export = router;
