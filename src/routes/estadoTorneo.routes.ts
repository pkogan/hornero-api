import { Router } from "express";
import {getEstado,createEstado,getEstados} from "../controllers/estadoTorneo.controller"
const router = Router();

router.get("/", getEstados);
router.get("/:id",getEstado);
router.post("/",createEstado);
/*
router.put("/",editLenguaje);
*/

export = router;
