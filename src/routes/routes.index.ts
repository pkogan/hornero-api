import {Router} from "express";
import * as torneos from './torneos.routes'
import * as login from './login.routes'
import * as lengujes from './lenguaje.routes'
import * as usuarios from './usuario.routes'
import * as estadoTorneo from './estadoTorneo.routes'
import * as tipoTorneo from './tipoTorneo.routes'
import * as stubs from './stubs.routes'
import * as rol from './rol.routes'
import * as jugar from './jugar.routes'; 
import * as problema from './problema.routes';
import * as complejidad from "./complejida.routes";
import * as etiqueta from "./etiqueta.routes";
import * as archivo from "./archivos.routes";
import * as codigo from './codigoUsuario.routes';
const router = Router();

router.use('/login',login);
router.use('/problema',problema);
router.use('/torneos',torneos);
router.use('/lenguajes',lengujes);
router.use('/usuarios',usuarios);
router.use('/estadoTorneo',estadoTorneo);
router.use('/tipoTorneo',tipoTorneo);
router.use('/stub',stubs);
router.use('/rol',rol);
router.use('/etiqueta',etiqueta);
router.use('/jugar',jugar);
router.use('/complejidad',complejidad);
router.use("/archivo",archivo);
router.use("/codigo",codigo);


export = router;