import { Router } from "express";
import { getRol,getRoles } from "../controllers/rol.controller";

const router = Router();

router.get("/", getRoles);
router.get("/:id", getRol);
//router.put("/",)
//router.post("/",createRol);

export = router;
